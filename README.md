# ARQUITECTURA DE COMPUTADORAS - SCD1003 - ISB

## VON-NEUMANN VS HARVARD

```plantuml
@startmindmap

* John Von Neumann :abacus:

** Quien era Von Neumann
*** Fisico Matematico\nNacio el 28 de diciembre en budapest, hungria,\nMurio el 8 de febrero de 1957 en washington Dc.\nRealizo varios aportes a las matematicas y creo la arquitectura de los computadores actuales

** logros
*** A la edad de 22 años recibio el titulo de ingeniero quimico por el instituto federal suizo de zurich
*** Un año mas tarde recibio un doctorado en matematicas en la universidad de budapest
*** en julio de 1945 desarrollo un programa que consistio en el almacenamiento en memoria de datos
*** creo la arquitectura de los computadores actuales
*** los modelos de von neumann y hardvard direron origen a la computacion


** Modelo de Von Neumann
*** En 1945 modificaron la ENIAC para que funcionara con la unidad de almacenamiento
*** En la memoria se encuentran las intrucciones y los datos
*** la ejecucion de instrucciones es de manera secuencial

** modelo de hardvard
*** Origen en 1945 con la computadora hardvard mark
*** Utilizaba dos memorias una contenia las instrucciones y otra la coleccion de datos
*** En la actualidad ya no se usa mucho por que su funcionamiento es mas dificil

@endmindmap
```

## Supercomputadoras en Mexico :desktop_computer:

```plantuml
@startmindmap

* Supercomputadoras en Mexico

** ¿Que es una super computadora?
*** Es una infraestructura que se diseña y se construye para procesar grandes cantidades de información

** UNAM
*** Fue la primera computadora latinoamericana en julio de 1958
*** Era la ibm-650 y usaba bulbos y tarjetas perforadas
*** Su uso principal fue la investigacion cientifica

** KANBALAM
*** Acual computadora de la unam
*** Su poder de computo equivale a mas de 1300 PC
*** Tiene una capacidad de 8344 procesadores

** MIZTU
*** En 2012 se creo xiuhcoatl por las universidades unam, uam  ipn
*** Predecir el clima, estudiar los sismos, diseñar nuevos materiales y fármacos son los usos que se le da
*** Tiene una capacidad de 8344 procesadores


** XIUCOALT
*** En 2012 se creo xiuhcoatl por las universidades unam, uam  ipn
*** Tiene dos caracteristicas
**** Nodos robustos de supercomputo en los centros de datos de las universidades
**** Tener una red de fibra optica que conecte las 3 instituciones a travez de las instalaciones del metro

** BUAP
*** Una de las 5 computadoras mas poderosas de latinoamericas
*** Tiene una capacidad de almacenamiento de 5000 computadoras portatiles 
*** tiene capacidad de hacer simulaciones poderosas


@endmindmap
```